﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Connection
{
    public class ORMCommand : IDBCommand
    {
        private IDBCommand m_Command;

        public ORMCommand(IDBCommand command)
        {
            m_Command = command;
        }

        public string CommandText
        {
            get
            {
                return m_Command.CommandText;
            }
            set
            {
                m_Command.CommandText = value;
            }
        }

        public ORMDataReader ExecuteReader()
        {
            return m_Command.ExecuteReader();
        }

        public int ExecuteNonQuery()
        {
            return m_Command.ExecuteNonQuery();
        }

        public void AddParameter(string parameter, object value)
        {
            m_Command.AddParameter(parameter, value);
        }

        public List<ORMParameter> GetParameters()
        {
            return m_Command.GetParameters();
        }

        public int FirstResult
        {
            get
            {
                return m_Command.FirstResult;
            }
            set
            {
                m_Command.FirstResult = value;
            }
        }

        public int MaxResults
        {
            get
            {
                return m_Command.MaxResults;
            }
            set
            {
                m_Command.MaxResults = value;
            }
        }


        public long LastInsertedId
        {
            get { return m_Command.LastInsertedId; }
        }
    }
}
