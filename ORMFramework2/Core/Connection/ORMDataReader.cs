﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Connection
{
    public class ORMDataReader
    {
        private IDBDataReader m_DataReader;

        public ORMDataReader(IDBDataReader dataReader)
        {
            m_DataReader = dataReader;
        }

        public bool Read()
        {
            return m_DataReader.Read();
        }

        public string GetString(string p)
        {
            return m_DataReader.GetString(p);
        }

        public DateTime GetDateTime(string p)
        {
            return m_DataReader.GetDateTime(p);
        }

        public int GetInt32(string p)
        {
            return m_DataReader.GetInt32(p);
        }

        public float GetFloat(string p)
        {
            return m_DataReader.GetFloat(p);
        }

        public double GetDouble(string p)
        {
            return m_DataReader.GetDouble(p);
        }

        public bool GetBoolean(string p)
        {
            return m_DataReader.GetBoolean(p);
        }

        public object GetValue(string field)
        {
            return m_DataReader.GetValue(field);
        }
    }
}
