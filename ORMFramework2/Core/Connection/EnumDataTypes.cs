﻿namespace ORMFramework2.Core.Connection
{
    public enum EnumDataTypes
    {
        STRING_TYPE, INT_TYPE, FLOAT_TYPE, DOUBLE_TYPE, DATETIME_TYPE, BOOLEAN_TYPE
    }
}
