﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Connection
{
    public class ORMParameter
    {
        public ORMParameter(string parameter, object value)
        {
            m_Parameter = parameter;
            m_Value = value;
        }

        private string m_Parameter;
        public string ParameterName
        {
            get { return m_Parameter; }
            set { m_Parameter = value; }
        }

        private object m_Value;
        public object Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }
    }
}
