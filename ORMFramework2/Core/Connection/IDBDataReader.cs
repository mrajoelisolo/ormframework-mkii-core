﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Connection
{
    public interface IDBDataReader
    {
        bool Read();

        string GetString(string field);
        DateTime GetDateTime(string field);
        int GetInt32(string field);
        float GetFloat(string field);
        double GetDouble(string field);
        bool GetBoolean(string field);
        object GetValue(string field);
    }
}
