﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Connection
{
    public interface IDBConnection
    {
        void Open();
        void Close();
        ORMCommand CreateCommand();
    }
}
