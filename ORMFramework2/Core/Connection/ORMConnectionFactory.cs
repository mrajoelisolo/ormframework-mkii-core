﻿using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework2.Core.Schemas.Config;
using ORMFramework2.Core.Libraries;
using MySql.Data.MySqlClient;
using ORMFramework2.Core.Adapters;
using System.Data.SqlClient;

namespace ORMFramework2.Core.Connection
{
    public class ORMConnectionFactory
    {
        private static ORMConnectionFactory m_Instance = new ORMConnectionFactory();

        private ORMConnectionFactory() { }

        public static ORMConnectionFactory Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public IDBConnection GetConnection(ORMConfig config)
        {
            IDBConnection res = null;

            EnumDBDriver dbdriver = ORMSchemaBrowser.Instance.GetDriverType(config);
            
            if (dbdriver == EnumDBDriver.MYSQL_DRIVER)
            {
                MySqlConnection con = new MySqlConnection(GetMySqlConnectionString(config));
                res = new MySqlConnectionAdapter(con);
            }
            else if (dbdriver == EnumDBDriver.MSSQL_DRIVER)
            {
                SqlConnection con = new SqlConnection(GetSqlConnectionString(config));
                res = new SqlConnectionAdapter(con);
            }

            return res;
        }

        public string GetMySqlConnectionString(ORMConfig config)
        {
            string server = ORMSchemaBrowser.Instance.GetUrl(config);
            string database = ORMSchemaBrowser.Instance.GetDatabaseName(config);
            string user = ORMSchemaBrowser.Instance.GetUserName(config);
            string password = ORMSchemaBrowser.Instance.GetPassword(config);
            string port = ORMSchemaBrowser.Instance.GetPort(config);

            string res = "SERVER=" + server + "; DATABASE=" + database + "; USER=" + user + "; PASSWORD=" + password + ";PORT=" + port;

            return res;
        }

        public string GetSqlConnectionString(ORMConfig config)
        {
            return ORMSchemaBrowser.Instance.GetUrl(config);
        }
    }
}
