﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Connection
{
    public interface IEntityField
    {
        string GetFieldName();
        void SetFieldName(string fieldName);
        EnumDataTypes GetFieldType();
        void SetFieldType(EnumDataTypes fieldType);
        string GetColName();
        void SetColName(string colName);
    }
}
