﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Connection
{
    public class ORMConnection
    {
        private IDBConnection m_Connection;

        public ORMConnection(IDBConnection connection)
        {
            m_Connection = connection;
        }

        public ORMConnection(string strCon)
        {
            
        }

        public ORMCommand CreateCommand()
        {
            return m_Connection.CreateCommand();
        }

        public void Open()
        {
            m_Connection.Open();
        }

        public void Close()
        {
            m_Connection.Close();
        }
    }
}
