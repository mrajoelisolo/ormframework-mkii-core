﻿using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework2.Core.Libraries;
using ORMFramework2.Core.Schemas.Config;

namespace ORMFramework2.Core.Connection
{
    public class ORMSessionFactory
    {
        private string m_ConfigFile;

        public ORMSessionFactory(string configFile)
        {
            m_ConfigFile = configFile;
        }

        public ORMSession CreateSession
        {
            get
            {
                ORMConfig schema = ORMSchemaSerializer.Instance.DeserializeConfig(m_ConfigFile);

                return new ORMSession(schema);
            }
        }

        public Boolean IsConnected
        {
            get
            {
                ORMConfig schema = ORMSchemaSerializer.Instance.DeserializeConfig(m_ConfigFile);
                IDBConnection cnx = ORMConnectionFactory.Instance.GetConnection(schema);
                Boolean res = false;

                try
                {
                    cnx.Open();
                    res = true;
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }

                return res;
            }
        }
    }
}
