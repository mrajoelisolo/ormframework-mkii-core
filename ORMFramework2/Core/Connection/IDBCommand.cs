﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Connection
{
    public interface IDBCommand
    {
        string CommandText { get; set; }
        ORMDataReader ExecuteReader();
        int ExecuteNonQuery();
        void AddParameter(string parameter, object value);
        List<ORMParameter> GetParameters();               
        int FirstResult { get; set; }
        int MaxResults { get; set; }
        long LastInsertedId { get; }
    }
}
