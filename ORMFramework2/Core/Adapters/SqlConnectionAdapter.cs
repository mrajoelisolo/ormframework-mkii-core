﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using ORMFramework2.Core.Connection;

namespace ORMFramework2.Core.Adapters
{
    public class SqlConnectionAdapter : IDBConnection
    {
        private SqlConnection m_Connection;

        public SqlConnectionAdapter(SqlConnection connection)
        {
            m_Connection = connection;
        }

        public void Open()
        {
            m_Connection.Open();
        }

        public void Close()
        {
            m_Connection.Close();
        }

        public ORMCommand CreateCommand()
        {
            SqlCommandAdapter res = new SqlCommandAdapter(m_Connection.CreateCommand());

            return new ORMCommand(res);
        }
    }
}
