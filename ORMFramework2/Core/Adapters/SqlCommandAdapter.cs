﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using ORMFramework2.Core.Connection;

namespace ORMFramework2.Core.Adapters
{
    public class SqlCommandAdapter : IDBCommand
    {
        private int m_FirstResult = -1;
        private int m_MaxResults = -1;

        private SqlCommand m_Command;

        public SqlCommandAdapter(SqlCommand command)
        {
            m_Command = command;
        }

        public ORMDataReader ExecuteReader()
        {
            SqlDataReaderAdapter res = new SqlDataReaderAdapter(m_Command.ExecuteReader());

            return new ORMDataReader(res);
        }

        public int ExecuteNonQuery()
        {
            return m_Command.ExecuteNonQuery();
        }


        public void AddParameter(string parameter, object value)
        {
            m_Command.Parameters.Add(new SqlParameter(parameter, value));
        }


        public List<ORMParameter> GetParameters()
        {
            List<ORMParameter> res = new List<ORMParameter>();

            foreach(SqlParameter parameter in m_Command.Parameters)
                res.Add(new ORMParameter(parameter.ParameterName, parameter.Value));

            return res;
        }


        public int FirstResult
        {
            get
            {
                return m_FirstResult;
            }
            set
            {
                m_FirstResult = value;
            }
        }

        public int MaxResults
        {
            get
            {
                return m_MaxResults;
            }
            set
            {
                m_MaxResults = value;
            }
        }

        public string CommandText
        {
            get
            {
                return m_Command.CommandText;
            }
            set
            {
                m_Command.CommandText = value;
            }
        }


        public long LastInsertedId
        {
            get {
                long res = -1;

                //Contrairement a Sql SqlCommand ne possede pas l'attribut last inserted id
                //Il faut donc faire une requete spécifique
                //Ref MSDN : ms-help://MS.MSDNQTR.v90.en/tsqlref9/html/912e4485-683c-41c2-97b3-8831c0289ee4.htm
                string query = "SELECT @@IDENTITY AS 'Last_Inserted_Id'";
                m_Command.CommandText = query;

                SqlDataReader reader = m_Command.ExecuteReader();
                while (reader.Read())
                    res = long.Parse(reader[0].ToString());

                return res;
            }
        }
    }
}
