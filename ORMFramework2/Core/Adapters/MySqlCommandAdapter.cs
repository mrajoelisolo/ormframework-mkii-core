﻿using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework2.Core.Connection;
using MySql.Data.MySqlClient;

namespace ORMFramework2.Core.Adapters
{
    public class MySqlCommandAdapter : IDBCommand
    {
        private MySqlCommand m_Command;
        private int m_FirstResult = -1;
        private int m_MaxResults = -1;

        public MySqlCommandAdapter(MySqlCommand command)
        {
            m_Command = command;
        }
       
        public ORMDataReader ExecuteReader()
        {
            MySqlDataReaderAdapter res = new MySqlDataReaderAdapter(m_Command.ExecuteReader());

            return new ORMDataReader(res);
        }

        public int ExecuteNonQuery()
        {
            return m_Command.ExecuteNonQuery();
        }

        public void AddParameter(string parameter, object value)
        {
            m_Command.Parameters.Add(new MySqlParameter(parameter, value));
        }


        public List<ORMParameter> GetParameters()
        {
            List<ORMParameter> res = new List<ORMParameter>();

            foreach(MySqlParameter parameter in m_Command.Parameters)
                res.Add(new ORMParameter(parameter.ParameterName, parameter.Value));

            return res;
        }


        public int FirstResult
        {
            get
            {
                return m_FirstResult;
            }
            set
            {
                m_FirstResult = value;
            }
        }

        public int MaxResults
        {
            get
            {
                return m_MaxResults;
            }
            set
            {
                m_MaxResults = value;
            }
        }

        public string CommandText
        {
            get
            {
                return m_Command.CommandText;
            }
            set
            {
                m_Command.CommandText = value;
            }
        }


        public long LastInsertedId
        {
            get { return m_Command.LastInsertedId; }
        }
    }
}
