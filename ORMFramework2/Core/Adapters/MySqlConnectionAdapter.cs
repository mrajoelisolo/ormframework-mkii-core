﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using ORMFramework2.Core.Connection;

namespace ORMFramework2.Core.Adapters
{
    public class MySqlConnectionAdapter : IDBConnection
    {
        private MySqlConnection m_Connection;

        public MySqlConnectionAdapter(MySqlConnection connection)
        {
            m_Connection = connection;
        }

        public void Open()
        {
            m_Connection.Open();
        }

        public void Close()
        {
            m_Connection.Close();
        }

        public ORMCommand CreateCommand()
        {
            MySqlCommandAdapter res = new MySqlCommandAdapter(m_Connection.CreateCommand());

            return new ORMCommand(res);
        }
    }
}
