﻿using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework2.Core.Connection;
using MySql.Data.MySqlClient;

namespace ORMFramework2.Core.Adapters
{
    public class MySqlDataReaderAdapter : IDBDataReader
    {
        private MySqlDataReader m_Reader;

        public MySqlDataReaderAdapter(MySqlDataReader reader)
        {
            m_Reader = reader;
        }

        public bool Read()
        {
            return m_Reader.Read();
        }

        public string GetString(string field)
        {
            return m_Reader.GetString(field);
        }

        public DateTime GetDateTime(string field)
        {
            return m_Reader.GetDateTime(field);
        }

        public int GetInt32(string field)
        {
            return m_Reader.GetInt32(field);
        }

        public float GetFloat(string field)
        {
            return m_Reader.GetFloat(field);
        }

        public double GetDouble(string field)
        {
            return m_Reader.GetDouble(field);
        }

        public bool GetBoolean(string field)
        {
            return m_Reader.GetBoolean(field);
        }


        public object GetValue(string field)
        {
            return m_Reader[field];
        }
    }
}
