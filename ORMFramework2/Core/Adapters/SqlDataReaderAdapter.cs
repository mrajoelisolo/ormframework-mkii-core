﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using ORMFramework2.Core.Connection;

namespace ORMFramework2.Core.Adapters
{
    public class SqlDataReaderAdapter : IDBDataReader
    {
        private SqlDataReader m_Reader;

        public SqlDataReaderAdapter(SqlDataReader reader)
        {
            m_Reader = reader;
        }

        public bool Read()
        {
            return m_Reader.Read();
        }

        public string GetString(string field)
        {
            int i = GetFieldId(field);

            return m_Reader.GetString(i);
        }

        public DateTime GetDateTime(string field)
        {
            int i = GetFieldId(field);

            return m_Reader.GetDateTime(i);
        }

        public int GetInt32(string field)
        {
            int i = GetFieldId(field);

            return m_Reader.GetInt32(i);
        }

        public float GetFloat(string field)
        {
            int i = GetFieldId(field);

            return m_Reader.GetFloat(i);
        }

        public double GetDouble(string field)
        {
            int i = GetFieldId(field);

            return m_Reader.GetDouble(i);
        }

        public bool GetBoolean(string field)
        {
            int i = GetFieldId(field);
            
            return m_Reader.GetBoolean(i);
        }

        public object GetValue(string field)
        {
            int i = GetFieldId(field);

            return m_Reader[i];
        }

        private int GetFieldId(string fieldName)
        {
            for (int i = 0; i < m_Reader.FieldCount; i++)
            {
                if (fieldName.ToLower().Equals(m_Reader.GetName(i)))
                    return i;
            }

            return -1;
        }
    }
}
