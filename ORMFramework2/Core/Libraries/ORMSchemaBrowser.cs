﻿using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework2.Core.Connection;
using ORMFramework2.Core.Schemas.Config;
using ORMFramework2.Core.Schemas.Mapping;

namespace ORMFramework2.Core.Libraries
{
    public class ORMSchemaBrowser
    {
        private static ORMSchemaBrowser m_Instance = new ORMSchemaBrowser();

        private ORMSchemaBrowser() { }

        public static ORMSchemaBrowser Instance
        {
            get
            {
                return m_Instance;
            }
        }

        #region "Config methods"
        public EnumDBDriver GetDriverType(ORMConfig config)
        {
            EnumDBDriver res = EnumDBDriver.T_UNDEFINED;
            
            ORMConfigProperty property = GetPropertyTag(config, "connection.dbdriver");

            if (property == null) return res;

            string driverName = property.PropertyValue.ToLower();

            if (driverName.Equals("mysql"))
                res = EnumDBDriver.MYSQL_DRIVER;
            else if (driverName.Equals("mssql"))
                res = EnumDBDriver.MSSQL_DRIVER;
            
            return res;
        }

        public string GetUrl(ORMConfig config)
        {
            ORMConfigProperty property = GetPropertyTag(config, "connection.url");

            if (property == null) return "";

            return property.PropertyValue;
        }

        public string GetDatabaseName(ORMConfig config)
        {
            ORMConfigProperty property = GetPropertyTag(config, "connection.database");

            if(property == null) return "";

            return property.PropertyValue;
        }

        public string GetUserName(ORMConfig config)
        {
            ORMConfigProperty property = GetPropertyTag(config, "connection.username");

            if (property == null) return "";

            return property.PropertyValue;
        }

        public string GetPassword(ORMConfig config)
        {
            ORMConfigProperty property = GetPropertyTag(config, "connection.password");

            if (property == null) return "";

            return property.PropertyValue;
        }

        public string GetPort(ORMConfig config)
        {
            ORMConfigProperty property = GetPropertyTag(config, "connection.port");

            if (property == null) return "";

            return property.PropertyValue;
        }

        public ORMConfigProperty GetPropertyTag(ORMConfig config, string propertyName)
        {
            foreach (ORMConfigProperty property in config.Properties)
            {
                if (property.PropertyName.ToLower().Equals(propertyName))
                    return property;
            }

            return null;
        }
        #endregion
        #region "Schema methods"
        public ORMEntity GetEntitySchema(List<ORMEntity> entities, Type type)
        {
            foreach (ORMEntity entity in entities)
                if (entity.ClassName.Equals(type.Name))
                    return entity;

            return null;
        }

        public List<ORMEntity> GetEntities(ORMConfig config)
        {
            List<ORMEntity> res = new List<ORMEntity>();

            foreach (ORMConfigResource resource in config.Resources)
            {
                ORMEntity entity = ORMSchemaSerializer.Instance.DeserializeEntity(resource.ResourcePath);
                res.Add(entity);
            }

            return res;
        }
        #endregion
    }
}
