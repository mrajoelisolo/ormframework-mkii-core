﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace ORMFramework2.Core.Libraries
{
    public class Reflector
    {
        private static Reflector m_instance = new Reflector();

        private Reflector() { }

        public static Reflector Instance
        {
            get
            {
                return m_instance;
            }
        }

        public Type ReadType(String assemblyString, String className)
        {
            Assembly asm = Assembly.Load(assemblyString);
            Type t = asm.GetType(assemblyString + "." + className);
            return t;
        }

        public Object CreateInstance(String assemblyString, String className)
        {
            Type t = ReadType(assemblyString, className);
            if (t == null)
                return null;

            return Activator.CreateInstance(t);
        }

        public Object CreateInstance(Type t)
        {
            if (t == null)
                return null;

            return Activator.CreateInstance(t);
        }

        public Object GetFieldValue(string fieldName, Object target)
        {
            if (target == null)
                throw new Exception("Target is not initialized.");

            Type t = target.GetType();

            return t.InvokeMember(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.GetField, null, target, null);
        }

        public void SetFieldValue(string fieldName, Object target, object value)
        {
            if (target == null)
                throw new Exception("Target is not initialized.");

            Type t = target.GetType();

            if (DBNull.Value.Equals(value))
                t.InvokeMember(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty, null, target, new Object[] { });
            else
            {
                FieldInfo f = t.GetField("");
                Type fieldType = f.FieldType;
                Object o = null;
                CheckAndTranslateEnum(value, fieldType, ref o);
                t.InvokeMember(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty, null, target, new Object[] { o });
            }
        }

        public Object GetPropertyValue(string propertyName, Object target)
        {
            if (target == null)
                throw new Exception("Target is not initialized.");

            Type t = target.GetType();
            return t.InvokeMember(propertyName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty, null, target, null);
        }

        public void SetPropertyValue(string propertyName, Object target, object value)
        {
            if (target == null)
                throw new Exception("Target is not initialized.");

            Type t = target.GetType();

            PropertyInfo pr = t.GetProperty(propertyName);
            Type prType = pr.PropertyType;
            Object o = null;
            CheckAndTranslateEnum(value, prType, ref o);
            t.InvokeMember(propertyName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty, null, target, new Object[] { o });
        }

        private void CheckAndTranslateEnum(Object value, Type prType, ref Object o)
        {
            if (value != null)
            {
                if (prType.IsEnum)
                    o = System.Convert.ChangeType(value, typeof(Int32));
                else
                {
                    try
                    {
                        o = System.Convert.ChangeType(value, prType);
                    }
                    catch (Exception ex)
                    {
                        o = null;
                        System.Console.WriteLine(ex.Message);
                    }
                }
            }
        }
    }
}
