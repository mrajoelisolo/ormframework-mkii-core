﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using ORMFramework2.Core.Schemas;
using ORMFramework2.Core.Schemas.Mapping;
using ORMFramework2.Core.Schemas.Config;

namespace ORMFramework2.Core.Libraries
{
    public class ORMSchemaSerializer
    {
        private static ORMSchemaSerializer m_Instance = new ORMSchemaSerializer();       

        private ORMSchemaSerializer() { }

        public static ORMSchemaSerializer Instance
        {
            get
            {
                return m_Instance;
            }
        }

        #region "Generic methods"
        public void Serialize(object schema, string path)
        {
            try
            {
                XmlSerializer writer = new XmlSerializer(schema.GetType());
                StreamWriter file = new StreamWriter(path);
                writer.Serialize(file, schema);
                file.Close();
            }
            catch (Exception ex)
            {
                System.Console.Write(ex.Message);
            }
        }

        public object Deserialize(Type type, string path)
        {
            object res = null;

            try
            {
                XmlSerializer reader = new XmlSerializer(type.GetType());
                StreamReader file = new StreamReader(path);
                res = reader.Deserialize(file);
                file.Close();
            }
            catch (Exception ex)
            {
                res = new ORMSchema();
                System.Console.WriteLine(ex.Message);
            }

            return res;
        }
        #endregion

        public void SerializeEntity(ORMEntity entity, string path)
        {
            Serialize(entity, path);
        }

        public ORMEntity DeserializeEntity(string path)
        {
            ORMEntity res = null;

            try
            {
                XmlSerializer reader = new XmlSerializer(typeof(ORMEntity));
                StreamReader file = new StreamReader(path);
                res = (ORMEntity)reader.Deserialize(file);
                file.Close();
            }
            catch (Exception ex)
            {
                res = new ORMEntity();
                System.Console.WriteLine(ex.Message);
            }

            return res;
        }

        public void SerializeConfig(ORMConfig config, string path)
        {
            Serialize(config, path);
        }

        public ORMConfig DeserializeConfig(string path)
        {
            ORMConfig res = null;

            try
            {
                XmlSerializer reader = new XmlSerializer(typeof(ORMConfig));
                StreamReader file = new StreamReader(path);
                res = (ORMConfig)reader.Deserialize(file);
                file.Close();
            }
            catch (Exception ex)
            {
                res = new ORMConfig();
                System.Console.WriteLine(ex.Message);
            }

            return res;
        }
    }
}
