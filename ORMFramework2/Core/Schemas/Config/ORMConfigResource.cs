﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Schemas.Config
{
    public class ORMConfigResource
    {
        private string m_ResourcePath;
        public string ResourcePath
        {
            get { return m_ResourcePath; }
            set { m_ResourcePath = value; }
        }
    }
}
