﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Schemas.Config
{
    public class ORMConfigProperty
    {
        private string m_PropertyName;
        public string PropertyName
        {
            get { return m_PropertyName; }
            set { m_PropertyName = value; }
        }

        private string m_PropertyValue;
        public string PropertyValue
        {
            get { return m_PropertyValue; }
            set { m_PropertyValue = value; }
        }
    }
}
