﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Schemas.Config
{
    public class ORMConfig
    {
        private List<ORMConfigProperty> m_Properties = new List<ORMConfigProperty>();
        public List<ORMConfigProperty> Properties
        {
            get { return m_Properties; }
        }

        private List<ORMConfigResource> m_Resources = new List<ORMConfigResource>();
        public List<ORMConfigResource> Resources
        {
            get { return m_Resources; }
        }
    }
}
