﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Schemas.Mapping
{
    public class ORMEntity
    {
        private string m_ClassName;
        public string ClassName
        {
            get { return m_ClassName; }
            set { m_ClassName = value; }
        }

        private string m_TableName;
        public string TableName
        {
            get { return m_TableName; }
            set { m_TableName = value; }
        }

        private ORMEntityField m_IdField;
        public ORMEntityField IdField
        {
            get { return m_IdField; }
            set { m_IdField = value; }
        }

        private List<ORMEntityField> m_Fields = new List<ORMEntityField>();
        public List<ORMEntityField> Fields
        {
            get { return m_Fields; }
        }
    }
}
