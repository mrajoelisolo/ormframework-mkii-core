﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Schemas.Mapping
{
    public class ORMSchema
    {
        private List<ORMEntity> m_Entities = new List<ORMEntity>();
        public List<ORMEntity> Entities
        {
            get
            {
                return m_Entities;
            }
        }
    }
}
