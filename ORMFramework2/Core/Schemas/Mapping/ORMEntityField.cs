﻿using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework2.Core.Connection;

namespace ORMFramework2.Core.Schemas.Mapping
{
    public class ORMEntityField
    {
        private string m_FieldName;
        public string FieldName
        {
            get { return m_FieldName; }
            set { m_FieldName = value; }
        }

        private EnumDataTypes m_FieldType;
        public EnumDataTypes FieldType
        {
            get { return m_FieldType; }
            set { m_FieldType = value; }
        }

        private string m_ColName;
        public string ColName
        {
            get { return m_ColName; }
            set { m_ColName = value; }
        }
    }
}
