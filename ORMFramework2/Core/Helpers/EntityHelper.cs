﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework2.Core.Helpers
{
    public class EntityHelper
    {
        private static EntityHelper m_Instance = new EntityHelper();

        private EntityHelper() { }

        public static EntityHelper Instance
        {
            get { return m_Instance; }
        }
    }
}
