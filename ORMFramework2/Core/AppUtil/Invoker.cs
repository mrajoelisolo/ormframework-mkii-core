using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework.AppUtil
{
    public class Invoker
    {
        private static Invoker m_Instance = new Invoker();

        private Invoker() { }

        public static Invoker Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public void Invoke(ICommand cmd)
        {
            if (cmd == null) return;

            cmd.Doing();
        }
    }
}
